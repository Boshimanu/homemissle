using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour {

	public float speed = 1f;
    public GameObject explosion;
    public GameObject [] missles;
    public GameObject audioController;
    AudioSource audioSource;
    
    private Camera camera;
    Vector3 point;
   
    private void Start() {
       
        missles = GameObject.FindGameObjectsWithTag("Missle");
        audioController = GameObject.Find("AudioController");
        audioSource = audioController.GetComponent<AudioSource>();
        
        camera = Camera.main;
        point = camera.ScreenToWorldPoint(new Vector3(Screen.widith, Screen.hieght, camera.nearClipPlane));
        Debug.Log(point.x);
	    Debug.Log(point.y); 
    }

    private void Update() {
		float horizontal = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
		float vertical = Input.GetAxis("Vertical") * speed * Time.deltaTime;

		// if ((transform.position.x<MAX_X) && (transform.position.y<MAX_Y)){
        	transform.Translate(Mathf.Clamp(horizontal,-point.x,point.x), vertical, 0f);
		// }
    }

    private void OnTriggerEnter2D() {
        audioSource.Play();

        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(gameObject);
        
        foreach (GameObject missle in missles) {
            Destroy(missle);
        }
    }

}