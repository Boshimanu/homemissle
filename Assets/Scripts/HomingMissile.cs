using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class HomingMissile : MonoBehaviour {

	public Transform target;
	public float speed = 6f;
	public float rotateSpeed = 600f;
	private Rigidbody2D rb;


	// Use this for initialization
	void Start () {
        Time.timeScale = 0f;
		rb = GetComponent<Rigidbody2D>();
	}

    private void Update() {
        if (Time.timeScale == 0f && Input.anyKey && !Input.GetKey(KeyCode.Mouse0) && !Input.GetKey(KeyCode.Mouse1)) {
            Time.timeScale = 1.0f;
        }
    }

    void FixedUpdate () {
        if (target & rb) {
            Vector2 direction = (Vector2)target.position - rb.position;
            direction.Normalize();
            float rotateAmount = Vector3.Cross(direction, transform.up).z;
            rb.angularVelocity = -rotateAmount * rotateSpeed;
           // rb.velocity = transform.up * speed;
        }
	}
}
